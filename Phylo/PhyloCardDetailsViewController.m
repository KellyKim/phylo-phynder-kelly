//
//  PhyloCardDetailsViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCardDetailsViewController.h"

@interface PhyloCardDetailsViewController ()

@end

@implementation PhyloCardDetailsViewController
@synthesize cardWebView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [cardWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://phylogame.org/2013/04/30/evening-grosbeak/"]]];
    
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    NSString *currentURL = webView.request.URL.absoluteString;
    NSLog(@"%@", currentURL);
    if([currentURL isEqualToString:@"http://phylogame.org/2013/04/30/evening-grosbeak/"])
        [webView stringByEvaluatingJavaScriptFromString:@"scrollTo(100,100);"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end