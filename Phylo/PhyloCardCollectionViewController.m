//
//  PhyloCardCollectionViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloCardCollectionViewController.h"

@interface PhyloCardCollectionViewController ()

@end

@implementation PhyloCardCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cardDetailsPressed:(id)sender{
    PhyloCardDetailsViewController *cardDetailsViewController = [[PhyloCardDetailsViewController alloc] initWithNibName:@"PhyloCardDetailsViewController" bundle:nil];
    [self.navigationController pushViewController:cardDetailsViewController animated:YES];
}

@end
