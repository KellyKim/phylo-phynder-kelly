//
//  PhyloMyStatsViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloMyStatsViewController.h"

@interface PhyloMyStatsViewController ()

@end

@implementation PhyloMyStatsViewController
@synthesize statsTable;
//@synthesize cardCollection;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"My Stats", @"My Stats"); //the text under the tab
        self.tabBarItem.image = [UIImage imageNamed:@"second"]; //the logo of the tab
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { //number of sections in table
    return 1;
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *sectionName;
    switch(section){
        case 0:
            sectionName = NSLocalizedString(@"My profile statistics", @"My profile statistics");
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

//Customize the appearance of table view cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {//
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"profile" ofType:@"txt"];
    NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];
    
    NSArray *contentItems = [content componentsSeparatedByString:@":"];
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
   // UITableViewCell *rightText = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
        //rightText = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }
    
    //set up the cell
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    if([indexPath row] == 0){ //there is, without a doubt, a more efficient way to implement row items.
        cell.textLabel.text = [NSString stringWithFormat:@"Cards collected:"];
        cell.detailTextLabel.text = contentItems[0];
     //   rightText.textLabel.text = [NSString stringWithFormat:@"Cards collected:"];
    }
    else if ([indexPath row] == 1){
        cell.textLabel.text = [NSString stringWithFormat:@"Total points:"];
        cell.detailTextLabel.text = contentItems[1];
    }
    else if ([indexPath row] == 2){
        cell.textLabel.text = [NSString stringWithFormat:@"Total number of games played:"];
        cell.detailTextLabel.text = contentItems[2];
    }
    else if ([indexPath row] == 3){
        cell.textLabel.text = [NSString stringWithFormat:@"Member since:"];
        cell.detailTextLabel.text = contentItems[3];
    }
    else if ([indexPath row] == 4){
        cell.textLabel.text = [NSString stringWithFormat:@"Latest discovered card:"];
        cell.detailTextLabel.text = contentItems[4];
    }
    
    return cell;
}

-(IBAction)cardCollectionPressed:(id)sender{
    //MapViewController *mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    //[self.navigationController pushViewController:mapViewController animated:YES];//
    
    PhyloCardCollectionViewController *cardCollectionViewController = [[PhyloCardCollectionViewController alloc] initWithNibName:@"PhyloCardCollectionViewController" bundle:nil];
    [self.navigationController pushViewController:cardCollectionViewController animated:YES];
}

@end
