//
//  PhyloPublicGames.h
//  Phylo
//
//  Created by Lydia Huang on 2013-06-22.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhyloPublicGames : NSObject{
    NSString *gameName;
}

@property (nonatomic,copy) NSString *gameName;

+ (id)gameNameCreate:(NSString*)name;

@end
