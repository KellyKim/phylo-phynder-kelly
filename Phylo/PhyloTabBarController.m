//
//  PhyloTabBarController.m
//  Phylo
//
//  Created by Jacky Chao on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloTabBarController.h"
#import "PhyloLoginViewController.h"

@interface PhyloTabBarController ()

@end

@implementation PhyloTabBarController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    PhyloLoginViewController* logVC = [[PhyloLoginViewController alloc]init];
    [self presentViewController:logVC animated:YES completion:nil];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
