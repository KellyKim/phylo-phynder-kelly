//
//  PhyloCardCollectionViewController.h
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloCardDetailsViewController.h"

@interface PhyloCardCollectionViewController : UIViewController

-(IBAction)cardDetailsPressed:(id)sender;

@end
