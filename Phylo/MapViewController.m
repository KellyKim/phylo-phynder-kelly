//
//  MapViewController.m
//  Phylo
//
//  Created by Kelly Kim on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "MapViewController.h"
#import "MapViewAnnotation.h"

@implementation MapViewController
@synthesize mapView;

//CORE LOCATION METHOD
//-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

//    if (self) {
//Create location manager object
//        locationManager = [[CLLocationManager alloc]init];
//accuracy = most accurate as possible
//        [locationManager setDesiredAccuracy:(kCLLocationAccuracyBest)];

//setting delegate
//        [locationManager setDelegate:self];
//    }
//    return self;
//}

//print device location
//-(void)locationManager:(CLLocationManager*)manager
//   didUpdateToLocation:(CLLocation *)newLocation
//          fromLocation:(CLLocation *)oldLocation
//{
//    NSLog(@"%@",newLocation);
//}

//Location manager fails to find location
//-(void)locationManager:(CLLocationManager*)manager
//      didFailWithError:(NSError *)error
//{
//    NSLog(@"Could not find location: %@",error);
//}





// When a map annotation point is added, zoom to it (500 range)
- (void)mapView:(MKMapView *)mv didAddAnnotationViews:(NSArray *)views
{
	MKAnnotationView *annotationView = [views objectAtIndex:0];
	id <MKAnnotation> mp = [annotationView annotation];
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance([mp coordinate], 250, 250);
	[mv setRegion:region animated:YES];
	[mv selectAnnotation:mp animated:YES];
}

// Received memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


// Deallocations
- (void)dealloc {
	[mapView release];
    [super dealloc];
}


CLLocationManager *_locationManager;
NSArray *_regionArray;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeLocationManager];
    NSArray *geofences = [self buildGeofenceData];
    [self initializeRegionMonitoring:geofences];
    [self initializeLocationUpdates];
    
    // set coordinates for position koi pond
    CLLocationCoordinate2D num1;
	num1.latitude = (double) 49.278982;
	num1.longitude = (double) -122.916969;
    
    CLLocationCoordinate2D num2;
	num2.latitude = (double) 49.279103;
	num2.longitude = (double) -122.922013;
    
    CLLocationCoordinate2D num3;
	num3.latitude = (double) 49.280239;
	num3.longitude = (double) -122.9294;
    
    CLLocationCoordinate2D num4;
	num4.latitude = (double) 49.2764;
	num4.longitude = (double) -122.904498;
    
    // Add the koi pond annotation to our map view
	MapViewAnnotation *newAnnotation1 = [[MapViewAnnotation alloc] initWithTitle:@"KOI POND WALK WAY" andCoordinate:num1];
	[self.mapView addAnnotation:newAnnotation1];
	[newAnnotation1 release];
    
    // Add the entrance to education annotation to our map view
	MapViewAnnotation *newAnnotation2 = [[MapViewAnnotation alloc] initWithTitle:@"education building" andCoordinate:num2];
	[self.mapView addAnnotation:newAnnotation2];
	[newAnnotation2 release];
    
    // Add the Convo Mall annotation to our map view
	MapViewAnnotation *newAnnotation3 = [[MapViewAnnotation alloc] initWithTitle:@"Convo Mall" andCoordinate:num3];
	[self.mapView addAnnotation:newAnnotation3];
	[newAnnotation3 release];
    
    // Add the Terry Fox annotation to our map view
	MapViewAnnotation *newAnnotation4 = [[MapViewAnnotation alloc] initWithTitle:@"Terry Fox" andCoordinate:num4];
	[self.mapView addAnnotation:newAnnotation4];
	[newAnnotation4 release];

}

- (void)viewDidUnload
{
   
    [self setMapView:nil];
    [super viewDidUnload];
    [super viewDidUnload];
	[mapView release];
	mapView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
/*
- (void)initializeMap {
    CLLocationCoordinate2D initialCoordinate;
    initialCoordinate.latitude = 41.88072;
    initialCoordinate.longitude = -87.67429;
    
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(initialCoordinate, 400, 400) animated:YES];
    self.mapView.centerCoordinate = initialCoordinate;
    [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
}
 */

- (void)initializeLocationManager {
    // Check to ensure location services are enabled
    if(![CLLocationManager locationServicesEnabled]) {
        [self showAlertWithMessage:@"You need to enable location services to use this app."];
        return;
    }
    
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;
}


- (void) initializeRegionMonitoring:(NSArray*)geofences {
    
    if (_locationManager == nil) {
        [NSException raise:@"Location Manager Not Initialized" format:@"You must initialize location manager first."];
    }
    
    if(![CLLocationManager regionMonitoringAvailable]) {
        [self showAlertWithMessage:@"This app requires region monitoring features which are unavailable on this device."];
        return;
    }
    
    for(CLRegion *geofence in geofences) {
        [_locationManager startMonitoringForRegion:geofence];
    }
    
}

- (NSArray*) buildGeofenceData {
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"regions" ofType:@"plist"];
    _regionArray = [NSArray arrayWithContentsOfFile:plistPath];
    
    NSMutableArray *geofences = [NSMutableArray array];
    for(NSDictionary *regionDict in _regionArray) {
        CLRegion *region = [self mapDictionaryToRegion:regionDict];
        [geofences addObject:region];
    }
    
    return [NSArray arrayWithArray:geofences];
}
//create regions from the dictionaries
- (CLRegion*)mapDictionaryToRegion:(NSDictionary*)dictionary {
    NSString *title = [dictionary valueForKey:@"title"];
    
    CLLocationDegrees latitude = [[dictionary valueForKey:@"latitude"] doubleValue];
    CLLocationDegrees longitude =[[dictionary valueForKey:@"longitude"] doubleValue];
    CLLocationCoordinate2D centerCoordinate = CLLocationCoordinate2DMake(latitude, longitude);
    
    CLLocationDistance regionRadius = [[dictionary valueForKey:@"radius"] doubleValue];
    
    return [[CLRegion alloc] initCircularRegionWithCenter:centerCoordinate
                                                   radius:regionRadius
                                               identifier:title];
}

- (void)initializeLocationUpdates {
    [_locationManager startUpdatingLocation];
}

#pragma mark - Location Manager - Region Task Methods

- (void)locationManager:(CLLocationManager *)manager didEnterRegion:(CLRegion *)region {
    CLLocation *userLoc = mapView.userLocation.location;
    //CLLocationCoordinate2D userCoor = userLoc.coordinate;
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:userLoc.coordinate.latitude longitude:userLoc.coordinate.longitude];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:region.center.latitude longitude:region.center.longitude];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    NSLog(@"distance: %f, locA: %f,%f, locB: %f,%f",distance,locA.coordinate.latitude,locA.coordinate.longitude,locB.coordinate.latitude,locB.coordinate.longitude);
    
    //CLLocationDistance distance = [userLoc distanceFromLocation:region];
    
    //if( region.center.latitude )
    //NSLog(@"%@ 's distance from %@ is: %f",userLoc ,region,[userLoc distanceFromLocation:region]/1000);
    if(distance <= 2.0){
        NSLog(@"Entered Region - %@", region.identifier);
        [self showRegionAlert:@"Entering Region" forRegion:region.identifier];
    }
}
/*
- (void)locationManager:(CLLocationManager *)manager didExitRegion:(CLRegion *)region {
    CLLocation *userLoc = mapView.userLocation.location;
    
    CLLocation *locA = [[CLLocation alloc] initWithLatitude:userLoc.coordinate.latitude longitude:userLoc.coordinate.longitude];
    CLLocation *locB = [[CLLocation alloc] initWithLatitude:region.center.latitude longitude:region.center.longitude];
    CLLocationDistance distance = [locA distanceFromLocation:locB];
    NSLog(@"distance: %f, locA: %f,%f, locB: %f,%f",distance,locA.coordinate.latitude,locA.coordinate.longitude,locB.coordinate.latitude,locB.coordinate.longitude);
    
    if(distance > 10.0){
        NSLog(@"Exited Region - %@", region.identifier);
        [self showRegionAlert:@"Exiting Region" forRegion:region.identifier];

    }
}
*/
- (void)locationManager:(CLLocationManager *)manager didStartMonitoringForRegion:(CLRegion *)region {
    NSLog(@"Started monitoring %@ region", region.identifier);
}


#pragma mark - Alert Methods

- (void) showRegionAlert:(NSString *)alertText forRegion:(NSString *)regionIdentifier {
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:alertText
                                                      message:regionIdentifier
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
}

- (void)showAlertWithMessage:(NSString*)alertText {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Error"
                                                        message:alertText
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
    [alertView show];
}


@end
