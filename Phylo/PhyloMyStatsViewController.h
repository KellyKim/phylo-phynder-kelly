//
//  PhyloMyStatsViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloCardCollectionViewController.h"

@interface PhyloMyStatsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView* statsTable;
//    IBOutlet UIButton* cardCollection;
}

@property (nonatomic, retain) UITableView* statsTable;
//@property (nonatomic, retain) UIButton* cardCollection;
-(IBAction)cardCollectionPressed:(id)sender;

@end
