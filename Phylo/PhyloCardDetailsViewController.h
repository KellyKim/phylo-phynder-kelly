//
//  PhyloCardDetailsViewController.h
//  Phylo
//
//  Created by Cody Santos on 6/22/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhyloCardDetailsViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UIWebView *cardWebView;
}

@property (nonatomic, retain) IBOutlet UIWebView *cardWebView;

@end
