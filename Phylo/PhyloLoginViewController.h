//
//  PhyloLoginViewController.h
//  Phylo
//
//  Created by Jacky Chao on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"


@interface PhyloLoginViewController : UIViewController <UITextFieldDelegate> 

@property (strong, nonatomic) IBOutlet UITextField *nameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) UITabBarController *tabVC;
- (IBAction)backgroundClick:(id)sender;
- (IBAction)Login:(UIButton *)sender;
- (IBAction)newUser:(UIButton *)sender;
- (void)requestFinished:(ASIHTTPRequest*)request;
- (void)requestFailed:(ASIHTTPRequest*)request;

- (void)authenticateHandler:(NSNotification *)notification;

@end
