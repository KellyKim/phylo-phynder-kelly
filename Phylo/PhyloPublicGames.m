//
//  PhyloPublicGames.m
//  Phylo
//
//  Created by Lydia Huang on 2013-06-22.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPublicGames.h"

@implementation PhyloPublicGames
@synthesize gameName;

+ (id) gameNameCreate:(NSString *)name{
    PhyloPublicGames *newGame = [[self alloc]init];
    newGame.gameName = name;
    return newGame;
}



@end
