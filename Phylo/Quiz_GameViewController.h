//
//  Quiz_GameViewController.h
//  Quiz Game
//
//

#import <UIKit/UIKit.h>

@interface Quiz_GameViewController : UIViewController {
	IBOutlet	UILabel		*theQuestion;
	IBOutlet	UILabel		*theScore;
	IBOutlet	UIButton	*answerOne;
	IBOutlet	UIButton	*answerTwo;
	IBOutlet	UIButton	*answerThree;
	IBOutlet	UIButton	*answerFour;

    IBOutlet	UIButton	*extra;

    IBOutlet	UIImageView	*imageView;

    
	NSInteger myScore;
	NSInteger questionNumber;
	NSInteger rightAnswer;
	NSInteger time;
    NSInteger randomnumber;
	NSArray *theQuiz;
	NSTimer *timer;
	BOOL questionLive;
    BOOL nextGame;

}

@property (retain, nonatomic) UILabel	*theQuestion;
@property (retain, nonatomic) UILabel	*theScore;
@property (retain, nonatomic) UIButton	*answerOne;
@property (retain, nonatomic) UIButton	*answerTwo;
@property (retain, nonatomic) UIButton	*answerThree;
@property (retain, nonatomic) UIButton	*answerFour;
@property (retain, nonatomic) NSArray *theQuiz;
@property (retain, nonatomic) NSTimer *timer;
@property (retain, nonatomic) UIButton *extra;


-(IBAction)buttonOne;
-(IBAction)buttonTwo;
-(IBAction)buttonThree;
-(IBAction)buttonFour;



//-(IBAction)imagesGen:(id)sender;


-(void)checkAnswer;

-(void)askQuestion;

-(void)updateScore;

-(void)loadQuiz;

-(void)countDown;

@end

