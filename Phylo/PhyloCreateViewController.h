//
//  PhyloCreateViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-04.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "PhyloSelectMapViewController.h"

@interface PhyloCreateViewController : UIViewController <UITextFieldDelegate> {
    UISegmentedControl *privatePublic;
    UITextField *password;
    UITextField *nameOfScavengerHunt;
    NSString *string;
}

@property (nonatomic, retain) IBOutlet UISegmentedControl *privatePublic;
@property (nonatomic,retain) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *nameOfScavengerHunt;
@property (copy, nonatomic) NSString *string;

-(IBAction) dismissKeyboard:(id)sender;
-(IBAction) privatePublicIndexChanged;
-(IBAction) nextButtonPressed:(id)sender;

@end
