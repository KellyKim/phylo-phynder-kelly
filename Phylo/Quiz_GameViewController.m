//
//  Quiz_GameViewController.m
//  Quiz Game
//
//

#import "Quiz_GameViewController.h"

@implementation Quiz_GameViewController

@synthesize theQuestion, theScore, answerOne, timer,answerTwo, answerThree, answerFour, theQuiz,extra;


-(void)askQuestion
{
    
    // CREATES 4 RANDOM NUMBERS
    //randomnumber = arc4random() % 5;
    
    
	// Unhide all the answer buttons.
	[answerOne setHidden:NO];
	[answerTwo setHidden:NO];
	[answerThree setHidden:NO];
	[answerFour setHidden:NO];
	
	// Set the game to a "live" question (for timer purposes)
	questionLive = YES;
	
	// Set the time for the timer
	time = 100.0;
	

	// Go to the next question
	questionNumber = questionNumber + 1;
        
    //1 QUESTION GENERATOR
    NSInteger row;
	
	// We get the question from the questionNumber * the row that we look up in the array.
	
    switch(questionNumber){
        case 1:
            imageView.image = [UIImage imageNamed:@"penguin.jpg"];
            break;
        case 2:
            imageView.image = [UIImage imageNamed:@"Earthworm.jpg"];
            break;
        case 3:
            imageView.image = [UIImage imageNamed:@"giraffe.jpg"];
            break;
        case 4:
            imageView.image = [UIImage imageNamed:@"Moose.jpg"];
            break;

    }
	if(questionNumber == 1)
	{
		row = questionNumber - 1;
	}
	else
	{
		row = ((questionNumber - 1) * 6);
	}   	
	
	// Set the question string, and set the buttons the the answers
	NSString *selected = [theQuiz objectAtIndex:row];
	NSString *activeQuestion = [[NSString alloc] initWithFormat:@"Question: %@", selected];
	[answerOne setTitle:[theQuiz objectAtIndex:row+1] forState:UIControlStateNormal];
	[answerTwo setTitle:[theQuiz objectAtIndex:row+2] forState:UIControlStateNormal];
	[answerThree setTitle:[theQuiz objectAtIndex:row+3] forState:UIControlStateNormal];
	[answerFour setTitle:[theQuiz objectAtIndex:row+4] forState:UIControlStateNormal];
    
	rightAnswer = [[theQuiz objectAtIndex:row+5] intValue];
	
	// Set theQuestion label to the active question
	theQuestion.text = activeQuestion;
	
	// Start the timer for the countdown
	timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
	
	[selected release];
	[activeQuestion release];
    
}





// Check for the answer
-(void)checkAnswer:(int)theAnswerValue
{
	if(rightAnswer == theAnswerValue)
	{
		theQuestion.text = @"Correct!!";
		myScore = myScore + 10;
	}
	else
	{        
        NSInteger row;
        if(questionNumber == 1)
        {
            row = questionNumber - 1;
        }
        else
        {
            row = ((questionNumber - 1) * 6);
        }
        row = row +rightAnswer;
    	NSString *checkselected = [theQuiz objectAtIndex:row];
        NSString *checkQuestion = [[NSString alloc] initWithFormat:@"You are wrong, the answer is: %@", checkselected];

        
        theQuestion.text = checkQuestion;

        myScore = myScore - 10;
        [checkselected release];
        [checkQuestion release];
    }
	[self updateScore];
}

-(void)updateScore
{
	// If the score is being updated, the question is not live
	questionLive = NO;
	
	[self.timer invalidate];
	
	// Hide the answers from the previous question
	[answerOne setHidden:YES];
	[answerTwo setHidden:YES];
	[answerThree setHidden:YES];
	[answerFour setHidden:YES];

	NSString *scoreUpdate = [[NSString alloc] initWithFormat:@"Score: %d", myScore];
	theScore.text = scoreUpdate;
	[scoreUpdate release];
	
	// END THE GAME.
	NSInteger endOfQuiz = [theQuiz count];
	if((((questionNumber - 1) * 6) + 6) == endOfQuiz)
	{
		// Make button 1 appear as a next game button
       // nextGame = YES;
		[extra setHidden:NO];
		[extra setTitle:@"Lets start our next Adventure!" forState:UIControlStateNormal];
		
	}
	else
	{
        // Give a short rest between questions
        time = 3.0;
        
        // Initialize the timer
        timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(countDown) userInfo:nil repeats:YES];
	}
}





-(void)countDown
{
	// Question live counter
	if(questionLive==YES)
	{
		time = time - 1;
		if(time == 0)
		{
			questionLive = NO;
			theQuestion.text = @"Time's up! Sorry, but you do not get a card :'( ";
			myScore = myScore - 10;
			[self.timer invalidate];
			[self updateScore];
		}
	}
	// In-between Question counter
	else
	{
		time = time - 1;
	
		if(time == 0)
		{
			[self.timer invalidate];
			[self askQuestion];
		}
	}
	if(time < 0)
	{
		[self.timer invalidate];
	}
}


- (IBAction)buttonOne
{
	if(questionNumber == 0){
		// startup-state
		//start the game.
		[answerTwo setHidden:NO];
		[answerThree setHidden:NO];
		[answerFour setHidden:NO];
		[self askQuestion];
	}
	else
	{
		NSInteger theAnswerValue = 1;
		[self checkAnswer:(int)theAnswerValue];
        
	}
}

- (IBAction)buttonTwo
{
	NSInteger theAnswerValue = 2;
	[self checkAnswer:(int)theAnswerValue];
}

- (IBAction)buttonThree
{
	NSInteger theAnswerValue = 3;
	[self checkAnswer:(int)theAnswerValue];
}

- (IBAction)buttonFour
{
	NSInteger theAnswerValue = 4;
	[self checkAnswer:(int)theAnswerValue];
}








// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	questionLive = NO;
	
    theQuestion.text = @"Congratulation, You have reached a checkpoint";

	theScore.text = @"Score:0";
	questionNumber = 0;
	myScore = 0;
	[answerOne setTitle:@"Click here to start your quiz!" forState:UIControlStateNormal];
	[answerTwo setHidden:YES];
	[answerThree setHidden:YES];
	[answerFour setHidden:YES];
	[self loadQuiz];
}

-(void)loadQuiz
{
	NSArray *quizArray = [[NSArray alloc] initWithObjects:
    @"What kind of Animal is This?",@"Chicken",@"Penguin",@"Moose",@" Monkey",@"2",
    @"What kind of Animal is This?", @"Crocodile", @"Bird", @"Earthworm", @"Hamster", @"3",
    @"What kind of Animal is This?", @"Giraffe", @"Penguin", @"Dinosaur", @"Zebra", @"1",    
    @"What kind of Animal is This?", @"Horse", @"Dragonfly", @"Panda", @"Moose", @"4",nil];
	self.theQuiz = quizArray;
	[quizArray release];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
}


- (void)dealloc {
	[theQuestion release];
	[theScore release];
	[answerOne release];
	[answerTwo release];
	[answerThree release];
	[answerFour release];
	[theQuiz release];
	[self.timer release];
    [super dealloc];
}

@end
