//
//  PhyloPublicGamesViewController.h
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PhyloGameInfoViewController.h"
#import "MapViewController.h"
#import "PhyloPublicGames.h"

@interface PhyloPublicGamesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>
@property (strong,retain)NSMutableArray *tableData;
@property (strong,nonatomic)NSMutableArray *searchResults;
@property (strong, nonatomic) IBOutlet UISearchBar *search;
@property (strong, nonatomic) IBOutlet UITableView *searchesTable;


-(void)filterContentForSearchText:(NSString*)searchText;

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText;
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar;
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;

-(BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString *)searchString;
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption;



@end
