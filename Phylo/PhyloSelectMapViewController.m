//
//  PhyloSelectMapViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/18/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloSelectMapViewController.h"
#import "MapViewAnnotation.h"

@interface PhyloSelectMapViewController ()

@end

@implementation PhyloSelectMapViewController
@synthesize mapView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture:)];
    longPressGesture.minimumPressDuration = 2.0;
    [mapView addGestureRecognizer:longPressGesture];
    [longPressGesture release];
}

- (void) handleLongPressGesture:(UIGestureRecognizer*)gestureRecognizer{
    //if tap and hold has ended (ie. finger is lifted)
    
    if(gestureRecognizer.state != UIGestureRecognizerStateBegan){
        NSLog(@"aaaaaaa");
        return;
    }
    NSLog(@"BbbbbbbbbBBBb");
    //get CGPoint and convert to coordinates
    CGPoint point = [gestureRecognizer locationInView:mapView];
    CLLocationCoordinate2D locCoord = [mapView convertPoint:point toCoordinateFromView:mapView];
    
    //add coordinate to the map
    MapViewAnnotation *dropPin = [[MapViewAnnotation alloc] init];
    dropPin.coordinate = locCoord;
    [mapView addAnnotation:dropPin];
    [dropPin release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
