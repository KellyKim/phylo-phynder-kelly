//
//  PhyloPublicGamesViewController.m
//  Phylo
//
//  Created by Cody on 2013-06-02.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloPublicGamesViewController.h"

@interface PhyloPublicGamesViewController ()

@end

@implementation PhyloPublicGamesViewController{
}
@synthesize tableData;
@synthesize search;
@synthesize searchesTable;
@synthesize searchResults;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Public Games", @"Public Games");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
    }
    return self;
}
							
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableData = [[NSMutableArray alloc]init];
    self.tableData = [NSMutableArray arrayWithObjects:[PhyloPublicGames gameNameCreate:@"My Location"],[PhyloPublicGames gameNameCreate:@"Burnaby Campus Animal Hunt"],[PhyloPublicGames gameNameCreate:@"Surrey Campus Scavenger Hunt"],[PhyloPublicGames gameNameCreate:@"Fraser Valley Animal Frenzy"],[PhyloPublicGames gameNameCreate:@"Fun with Phylo @ SFU"],nil];
    
    //Initialize searches results;
    self.searchResults = [NSMutableArray arrayWithCapacity:[tableData count]];
    
    [self.searchesTable reloadData];
    
    //UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:[self.search resignFirstResponder]];
//    [self.view addGestureRecognizer:tap];
    
    
    [self.search setDelegate:self];

	// Do any additional setup after loading the view, typically from a nib.
}
- (void) viewDidAppear:(BOOL)animated{
    NSLog(@"hello");
    //[self.search becomeFirstResponder];
    //NSLog(@"%c",);
    //[super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

//Customize the number of rows in Table View
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searchesTable == self.searchDisplayController.searchResultsTableView)
        return [searchResults count];
    else
    return [self.tableData count];
}

//Customize the appearance of table view cells
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil){
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease] ;
    }
    
    //set up the cell data
    
    //JACKY'S VERSION 2013/06/22
    //temp instance
    PhyloPublicGames* publicGame = nil;
    if (searchesTable == self.searchDisplayController.searchResultsTableView){
        publicGame = [searchResults objectAtIndex:indexPath.row];
    } else{
        publicGame = [tableData objectAtIndex:indexPath.row];
    }
    publicGame = [self.tableData objectAtIndex:indexPath.row];
    //configure row cell
    cell.textLabel.text = publicGame.gameName;
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    
    
    //ORIGINAL
    /*cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    
    if([indexPath row] == 0)
        cell.textLabel.text = [NSString stringWithFormat:@"MyLocation"];//
    else if ([indexPath row] == 1)
        cell.textLabel.text = [NSString stringWithFormat:@"Burnaby Campus Animal Hunt"];
    else if ([indexPath row] == 2)
        cell.textLabel.text = [NSString stringWithFormat:@"Surrey Campus Scavenger Hunt"];
    else if ([indexPath row] == 3)
        cell.textLabel.text = [NSString stringWithFormat:@"Fraser Valley Animal Frenzy"];
    else if ([indexPath row] == 4)
        cell.textLabel.text = [NSString stringWithFormat:@"Fun with Phylo @ SFU"];*/
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //open an alert with an OK and cancel button
    
    /*
    NSString *alertString = [NSString stringWithFormat:@"Congrats! You're now in the game! Good luck!"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:alertString message:@"" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
    [alert show];
    [alert release];
    */
    
    /*
    PhyloGameInfoViewController* gameInfoViewController = [[PhyloGameInfoViewController alloc] initWithNibName:@"PhyloGameInfoViewController" bundle:nil];
    [self.navigationController pushViewController:gameInfoViewController animated:YES];
     */
    
    MapViewController *mapViewController = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    [self.navigationController pushViewController:mapViewController animated:YES];//
}
//


- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    NSLog(@"User searched for %@",searchBar.text);
    
    
}


- (void)dealloc {
    [search release];
    //[_searchesTable release];
    [super dealloc];
}


#pragma mark Content Filtering
-(void)filterContentForSearchText:(NSString *)searchText{
    if (searchText && searchText.length){
        [searchResults removeAllObjects];
        
        for(NSDictionary *dictionary in tableData){
            for (NSString *thiskey in [dictionary allkeys]){
                if ([thiskey isEqualToString:@"SearchKey1"])
                    NSLog(@"");
            }
        }
    }
}

//Search Delegates



//SearchDisplayController Delegates
-(BOOL)searchDisplayController:(UISearchDisplayController*)controller shouldReloadTableForSearchString:(NSString *)searchString{
    [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles]objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:[[self.searchDisplayController.searchBar scopeButtonTitles]objectAtIndex:searchOption]];
    return YES;
}


-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    [self filterContentForSearchText:searchText];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    searchBar.text=@"";
    
    searchResults = [tableData mutableCopy];
    [self.searchesTable reloadData];
    
    [searchBar resignFirstResponder];
    
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}


@end
