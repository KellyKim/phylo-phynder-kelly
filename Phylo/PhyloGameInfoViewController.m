//
//  PhyloGameInfoViewController.m
//  Phylo
//
//  Created by Cody Santos on 6/20/13.
//  Copyright (c) 2013 Cody. All rights reserved.
//

#import "PhyloGameInfoViewController.h"

@interface PhyloGameInfoViewController ()

@end

@implementation PhyloGameInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = NSLocalizedString(@"Game Info", @"Game Info");
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
